import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';

// You can import from local files
import Lab4 from './components/Lab4'; //Membuat Project React Native Pertama dengan expo-cli
import Lab5 from './components/Lab5'; //React Native Component
import Cafe from './components/Lab6'; //State & Event Bagian 1
import Latihan from './components/Lab7'; //State & Event Bagian 2
import Lab8 from './components/Lab8'; //Style
import FixedDimensionsBasics from './components/Lab9'; //Style Height & Width (Fixed Dimensions)
import FlexDimensionsBasics from './components/Lab10'; //Style Height & Width (Flex Dimensions)
import Lab11 from './components/Lab11'; //Style Height & Width (Flex Dimensions)
import Lab12 from './components/Lab12'; //Tugas Membuat Style
import Lab13 from './components/Lab13'; //Tugas Membuat Halaman Login
import Lab14 from './components/Lab14'; //React Navigation
import Lab15 from './components/Lab15'; //React Navigation 1

export default function App() {
  return (
    //<Lab4/>
    //<Lab5/>
    //<Cafe/>
    //<Latihan/>
    //<Lab8/>
    //<FixedDimensionsBasics></FixedDimensionsBasics>
    //<FlexDimensionsBasics/>
    //<Lab11/>
    //<Lab12/>
    //<Lab13/>
    //<Lab14/>
    <Lab15/>
  )
}