import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.tulisan1}>Hello I Am Bold</Text>
      <Text style={styles.tulisan2}>Hello I Am Italic</Text>
      <Text style={styles.tulisan3}>Hello I Am Underline</Text>
      <Text style={styles.tulisan4}>Hello I Am Big</Text>
      <Text style={styles.tulisan5}>Hello I Am Big And Have Color</Text>
    </View>
  );
}

export default App

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eaf2f2',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  tulisan1: {
    fontWeight: 'bold'
  },
  tulisan2: {
    fontStyle: 'italic'
  },
  tulisan3: {
    textDecorationLine: 'underline'
  },
  tulisan4: {
    fontSize: 23
  },
  tulisan5: {
    color: 'blue',
    fontSize: 23
  }
})