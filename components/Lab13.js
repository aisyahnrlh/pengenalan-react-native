import React from 'react';
import { Text, View, StyleSheet, Image, TextInput, Button, TouchableOpacity } from 'react-native';
import Logo from '../assets/logo.png';

function App() {
  return (
    <View style={styles.container}>
      <Image source={Logo} style={styles.gambarLogo}/>
      <Text style={styles.inputTitle}>LOGIN HERE</Text>
      <TextInput placeholder='Masukkan email' autoCompleteType={'email'} style={styles.inputText}/>
      <TextInput placeholder='Masukkan password' secureTextEntry={true} style={styles.inputText}/>
      <TouchableOpacity style={styles.tombol}>
        <View>
          <Text style={styles.tombolText}>LOGIN</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default App

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  gambarLogo: {
    width: 95,
    height: 95
  },
  inputTitle: {
    fontWeight: 'bold',
    color: '#549DC3',
    marginBottom: 10
  },
  inputText: {
    height: 40,
    width: 300,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    margin: 10,
    padding: 10
  },
  tombol: {
    height: 40,
    width: 300,
    backgroundColor: '#549DC3',
    borderRadius: 5,
    margin: 10,
  },
  tombolText: {
    color: 'white',
    fontWeight: 'bold',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    margin: 10
  }
})