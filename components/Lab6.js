import React, { useState } from 'react';
import { Button, Text, View } from 'react-native';

const Cat = (props) => {
  const [isHungry, setIsHungry] = useState(true)

  return (
    <View>
      <Text>
        Nama saya {props.name}, dan saya {isHungry ? "lapar" : "kenyang"}
      </Text>
      <Button
        onPress={() => {
          setIsHungry(false)
        }}
        disabled={!isHungry}
        title={isHungry ? "Beri ikan" : "Terima kasih!"}
      />
    </View>
  )
}

const Cafe = () => {
  return (
    <>
      <Cat name="Kitty" />
      <Cat name="Meong" />
    </>
  )
}

export default Cafe