import React, { useState } from 'react';
import { Button, Text, TextInput, View } from 'react-native';

const Latihan = (props) => {
  const [getNama, setNama] = useState('')
  const tampilAlert = () => {
    alert("Nama kamu = " + getNama)
  }
  return (
    <View>
      <Text>Siapa nama kamu</Text>
      <TextInput
        placeholder="Ketikkan nama"
        style={{width: 200, borderWidth:1}}
        onChangeText={text => setNama(text)} />
      <Text>Nama kamu: {getNama}</Text>
      <Button
        title="Submit"
        onPress={tampilAlert}
      />
    </View>
  )
}

export default Latihan