import React, { useState } from 'react';
import { Text, View, StyleSheet } from 'react-native';

function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Selamat Datang</Text>
      <Text style={styles.text}>Di</Text>
      <Text style={styles.text}>Aplikasi ABC</Text>
    </View>
  );
}

export default App

const styles=StyleSheet.create({
  container: {
    backgroundColor: '#ebebeb',
    alignItems: 'center'
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#43b2ec'
  }
})